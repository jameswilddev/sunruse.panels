window.SUNRUSE = window.SUNRUSE || {};
(function(){
    var templates = {
        panel: $([
            "<div class='sunruse-panels-panel'>",
                "<button class='sunruse-panels-panel-header-button sunruse-panels-previous' disabled='true'></button>",
                "<button class='sunruse-panels-panel-header-button sunruse-panels-next' disabled='true'></button>",
                "<button class='sunruse-panels-panel-header-button sunruse-panels-wrap-split-vertical'></button>",
                "<button class='sunruse-panels-panel-header-button sunruse-panels-wrap-split-horizontal'></button>",
                "<button class='sunruse-panels-panel-header-button sunruse-panels-wrap-tabs'></button>",
                "<button class='sunruse-panels-panel-header-button sunruse-panels-fullscreen'></button>",
                "<button class='sunruse-panels-panel-header-button sunruse-panels-restore'></button>",
                "<button class='sunruse-panels-panel-header-button sunruse-panels-close-tab'></button>",
                "<div class='sunruse-panels-content'>",
                "</div>",
            "</div>"].join("\n")),
        "tabs": $([
            "<div>",
                "<div class='sunruse-panels-title'>Tabs</div>",
                "<button class='sunruse-panels-panel-header-button sunruse-panels-add-tab'></button>",
                "<div class='sunruse-panels-panel sunruse-panels-active'></div>",
            "</div>"].join("\n")),
        "split-vertical": $([
            "<div>",
                "<div class='sunruse-panels-title'>Split</div>",
                "<div class='sunruse-panels-panel sunruse-panels-left'></div>",
                "<div class='sunruse-panels-panel sunruse-panels-right'></div>",
                "<div class='sunruse-panels-split-bar'>",
                    "<button class='sunruse-panels-merge-before'></button>",
                    "<button class='sunruse-panels-merge-after'></button>",
                    "<button class='sunruse-panels-turn'></button>",
                "</div>",
            "</div>"].join("\n")),
        "split-horizontal": $([
            "<div>",
                "<div class='sunruse-panels-title'>Split</div>",
                "<div class='sunruse-panels-panel sunruse-panels-top'></div>",
                "<div class='sunruse-panels-panel sunruse-panels-bottom'></div>",
                "<div class='sunruse-panels-split-bar'>",
                    "<button class='sunruse-panels-merge-before'></button>",
                    "<button class='sunruse-panels-merge-after'></button>",
                    "<button class='sunruse-panels-turn'></button>",
                "</div>",
            "</div>"].join("\n"))
    }
    
    // Call with a jQuery selector stating where to create a new SUNRUSE.panels 
    // viewport, the url of a page to load into new panels and jQuery elements
    // to load into panels upon a failure to fetch content.
    window.SUNRUSE.panels = function(viewport, newPanelUrl, failure) {
        var newPanel = function(callback){
            var output = templates.panel.clone()
            output.children(".sunruse-panels-content").load(newPanelUrl, function(response, status, xhr){
                if(status == "error")
                    $(this).empty().append(failure.clone())
                callback(output)
            })
        }   
        
        var dragging, previous
        $(document).ready(function(){
            newPanel(function(created) {
                viewport.append(created).click(function(e){
                    var target = $(e.target)
                    
                    var refreshHistory = function(from) {
                        from = from.closest(".sunruse-panels-panel")
                        if(from.children(".sunruse-panels-past").length) 
                            from.children(".sunruse-panels-previous").prop("disabled", false)
                        else
                            from.children(".sunruse-panels-previous").prop("disabled", true)
                        var next = from.children(".sunruse-panels-next")
                        if(from.children(".sunruse-panels-future").length) 
                            from.children(".sunruse-panels-next").prop("disabled", false)
                        else
                            from.children(".sunruse-panels-next").prop("disabled", true)                
                    }
                    
                    var wrapHandler = function(name, whenNot) {
                        if(!target.is(".sunruse-panels-wrap-" + name)) return whenNot()
                        var container = target.closest(".sunruse-panels-panel")
                        newPanel(function(created) {
                            var wrapper = templates[name].clone()
                            wrapper.find(".sunruse-panels-panel").append(created.clone().children())
                            wrapper.find(".sunruse-panels-panel").first().empty().append(container.children())
                            created.find(".sunruse-panels-content").empty().append(wrapper.children())
                            container.empty().append(created.children())
                            container.children(".sunruse-panels-previous, .sunruse-panels-next").remove()
                        })
                    }        
                    
                    var turnHandler = function(button, before, after, newBefore, newAfter, sizing, whenNot) {
                        if(!target.is(button)) return whenNot()
                        var container = target.closest(".sunruse-panels-content")
                        var b = container.children(".sunruse-panels-" + before)
                        var a = container.children(".sunruse-panels-" + after)
                        target.closest(".sunruse-panels-split-bar").css(after, "").css(before, "")
                            .css(newAfter, 100 * parseFloat(b.css(after)) / container[sizing]() + "%")
                        b.css(newAfter, 100 * parseFloat(b.css(after)) / container[sizing]() + "%").css(after, "").addClass("sunruse-panels-" + newBefore).removeClass("sunruse-panels-" + before)
                        a.css(newBefore, 100 * parseFloat(a.css(before)) / container[sizing]() + "%").css(before, "").addClass("sunruse-panels-" + newAfter).removeClass("sunruse-panels-" + after)
                    }
                    
                    var mergeHandler = function(direction, source, whenNot) {
                        if(!target.is(".sunruse-panels-merge-" + direction)) return whenNot()
                        var mergeFrom = target.closest(".sunruse-panels-content").children(source)
                        target.closest(".sunruse-panels-panel").empty().append(mergeFrom.children())
                    }        
                    
                    if(target.is("a:not(.sunruse-panels-external)")) {
                        var element
                        var panel = target.closest(".sunruse-panels-panel")
                        element = $("<div>").load(target.attr("href"), function(response, status, xhr){
                            if(status == "error")
                                element.empty().append(failure.clone())
                            panel.children(".sunruse-panels-future").remove()
                            panel.children(".sunruse-panels-content").addClass("sunruse-panels-past").removeClass("sunruse-panels-content")
                            panel.append($("<div>").addClass("sunruse-panels-content").append(element.children()))
                            refreshHistory(panel)
                        })
                        e.preventDefault()
                        return
                    } else {
                        if(target.is(".sunruse-panels-previous:not(:disabled)")) {
                            var panel = target.closest(".sunruse-panels-panel")
                            panel.children(".sunruse-panels-content").addClass("sunruse-panels-future").removeClass("sunruse-panels-content")
                            panel.children(".sunruse-panels-past").last().addClass("sunruse-panels-content").removeClass("sunruse-panels-past")
                            refreshHistory(panel)
                        } else {
                            if(target.is(".sunruse-panels-next:not(:disabled)")) {
                                var panel = target.closest(".sunruse-panels-panel")
                                panel.children(".sunruse-panels-content").addClass("sunruse-panels-past").removeClass("sunruse-panels-content")
                                panel.children(".sunruse-panels-future").first().addClass("sunruse-panels-content").removeClass("sunruse-panels-future")
                                refreshHistory(panel)
                            } else  
                                wrapHandler("split-vertical",
                                function(){wrapHandler("split-horizontal",
                                function(){wrapHandler("tabs",
                                function(){turnHandler(".sunruse-panels-panel.sunruse-panels-bottom ~ .sunruse-panels-split-bar > .sunruse-panels-turn", "bottom", "top", "left", "right", "height",
                                function(){turnHandler(".sunruse-panels-panel.sunruse-panels-right ~ .sunruse-panels-split-bar > .sunruse-panels-turn", "left", "right", "top", "bottom", "width",
                                function(){mergeHandler("before", ".sunruse-panels-right, .sunruse-panels-bottom",
                                function(){mergeHandler("after", ".sunruse-panels-left, .sunruse-panels-top",
                                function(){
                                    if(target.is(".sunruse-panels-fullscreen"))
                                        target.closest(".sunruse-panels-panel").addClass("sunruse-panels-fullscreen")
                                    else if(target.is(".sunruse-panels-restore"))
                                        target.closest(".sunruse-panels-panel").removeClass("sunruse-panels-fullscreen")
                                    else if(target.is(".sunruse-panels-add-tab")) {
                                        newPanel(function(created) {
                                            target.closest(".sunruse-panels-panel").children(".sunruse-panels-content").children().removeClass("sunruse-panels-active")
                                            target.closest(".sunruse-panels-panel").children(".sunruse-panels-content").append(created.addClass("sunruse-panels-active"))
                                        })
                                    } else if(target.is(".sunruse-panels-add-tab ~ .sunruse-panels-panel > .sunruse-panels-content > .sunruse-panels-title")) {
                                        target.closest(".sunruse-panels-panel").closest(".sunruse-panels-content").children(".sunruse-panels-panel").removeClass("sunruse-panels-active")
                                        target.closest(".sunruse-panels-panel").addClass("sunruse-panels-active")
                                    } else if(target.is(".sunruse-panels-close-tab")) {
                                        target.closest(".sunruse-panels-panel").remove()
                                    }
                                })})})})})})})
                        }
                    }
                }).on("mousedown", function(e){
                    var target = $(e.target)
                    
                    if(target.is(".sunruse-panels-split-bar")) {
                        dragging = target.addClass("sunruse-panels-dragging")
                        previous = dragging.is(".sunruse-panels-right ~ *") ? e.pageX : e.pageY
                    }
                    
                    if(target.is(".sunruse-panels-title")) {
                        dragging = target.closest(".sunruse-panels-panel").addClass("sunruse-panels-dragging")
                    }
                })
            })
            $(document).on("mousemove", function(e){
                if(!dragging) return
                var target = $(e.target)
                
                if(dragging.is(".sunruse-panels-panel")) {
                    $(".sunruse-panels-drag-target").removeClass("sunruse-panels-drag-target")
                    if(target.is(".sunruse-panels-panel *") && !target.closest(".sunruse-panels-panel").is(dragging.closest(".sunruse-panels-panel")) && !$.contains(dragging.closest(".sunruse-panels-panel")[0], target[0])) {
                        target.closest(".sunruse-panels-panel").addClass("sunruse-panels-drag-target")
                    }
                    return
                }
                
                var container = dragging.closest(".sunruse-panels-content")
                
                if(dragging.is(".sunruse-panels-right ~ *")) {
                    var limitBuffer = (dragging.width() * 100) / container.width()
                    var current = 100 * parseFloat(dragging.css("right")) / container.width()
                    current -= (e.pageX - previous) * 100 / container.width()
                    current = Math.min(Math.max(current, limitBuffer), 100 - limitBuffer)
                    dragging.siblings(".sunruse-panels-left").css("right", current + "%")
                    dragging.siblings(".sunruse-panels-right").css("left", 100 - current + "%")
                    dragging.css("right", current + "%")
                    previous = e.pageX
                } else {
                    var limitBuffer = (dragging.height() * 100) / container.height()
                    var current = 100 * parseFloat(dragging.css("bottom")) / container.height()
                    current -= (e.pageY - previous) * 100 / container.height()
                    current = Math.min(Math.max(current, limitBuffer), 100 - limitBuffer)
                    dragging.siblings(".sunruse-panels-top").css("bottom", current + "%")
                    dragging.siblings(".sunruse-panels-bottom").css("top", 100 - current + "%")
                    dragging.css("bottom", current + "%")
                    previous = e.pageY
                }
            }).on("mouseup", function(){
                if(!dragging) return
                
                var target = $(".sunruse-panels-drag-target")
                if(target.length) {
                    if($.contains(target[0], dragging[0])) {
                        var from = dragging.children()
                        target.empty().append(from)
                    } else {
                        var fromA = target.children()
                        var fromB = dragging.children()
                        target.empty().append(fromB)
                        dragging.empty().append(fromA)
                    }
                    target.removeClass("sunruse-panels-drag-target")
                }
        
                dragging.removeClass("sunruse-panels-dragging")
                dragging = null
            })
        })
    }
})()